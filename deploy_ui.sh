#!/bin/sh

echo "START DEPLOY"
ssh deploy@cloud.kaaiot.net "cd /var/www/cloud; git pull; npm run build"

echo "END"