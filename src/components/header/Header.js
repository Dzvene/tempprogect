import { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Link, withRouter } from 'react-router-dom';
import FlatButton from 'material-ui/FlatButton';
import IconButton from 'material-ui/IconButton';

import { logOut } from '../../redux/actions/autorization';

import './styles.less';

const logo = require('./../../assets/images/logo.svg'); // eslint-disable-line
const logout = require('./../../assets/images/logout.svg'); // eslint-disable-line

const propTypes = {
  logOut: PropTypes.func,
};

class Header extends PureComponent {
  renderLogo = () => (
    <IconButton containerElement={<Link to={'/'}/>}>
      <img src={logo}/>
    </IconButton>
  );

  render() {
    return (
      <div className="header flex between">
        <div className={'flex center'}>
          { this.renderLogo() }
        </div>
        <FlatButton
          icon={<img src={logout}/>}
          label="Log out"
          labelPosition="before"
          onClick={this.props.logOut()}
        />
      </div>
    );
  }
}

Header.propTypes = propTypes;

const mapDispatchToProps = (dispatch) => ({
  logOut: () => () => dispatch(logOut()),
});

export default withRouter(connect(null, mapDispatchToProps)(Header));

