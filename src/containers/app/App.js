import { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Switch, Route, withRouter, Redirect } from 'react-router-dom';

import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import getMuiTheme from 'material-ui/styles/getMuiTheme';
import lightBaseTheme from 'material-ui/styles/baseThemes/lightBaseTheme';

import Header from '../../components/header';
import Home from '../../containers/pages/home';
import NotFound from '../../containers/pages/not-found';
import Login from '../../containers/pages/login';

import '../../assets/less/normalize.less';
import '../../assets/less/global.less';
import './styles.less';


import { pagesUrl } from '../../constants/TextTemplates';

const propTypes = {
  authorized: PropTypes.bool,
};

const lightMuiTheme = getMuiTheme(lightBaseTheme);

class App extends PureComponent {
  static propTypes = {
    location: PropTypes.shape({
      pathname: PropTypes.string,
    }),
  };

  render() {
    const { pathname } = this.props.location;

    return (
      <MuiThemeProvider muiTheme={lightMuiTheme}>
        <div className={'app-wrapper flex left top'}>
          {pathname !== '/login' && <Header/>}
          {!this.props.authorized && <Redirect to={'/login'}/> }
          <Switch>
            <Route
              component={Home}
              exact
              path={pagesUrl.home}
            />
            <Route
              component={Login}
              path={pagesUrl.login}
            />
            <Route component={NotFound} />
          </Switch>
        </div>
      </MuiThemeProvider>
    );
  }
}

App.propTypes = propTypes;

const mapStateToProps = (state) => ({
  authorized: state.authorization.authorized,
});

export default withRouter(connect(mapStateToProps)(App));
