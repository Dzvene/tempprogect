import { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Redirect } from 'react-router-dom';
import TextField from 'material-ui/TextField';
import RaisedButton from 'material-ui/RaisedButton';

import { logIn } from '../../../redux/actions/autorization';
import { button } from '../../../constants/TextTemplates';

import './styles.less';

const propTypes = {
  authorized: PropTypes.bool,
  logIn: PropTypes.func,
};

const logo = require('./../../../assets/images/logo.svg'); // eslint-disable-line

class Login extends PureComponent {
  state = {
    isErrorLogin: false,
    isErrorPassword: false,
    isLogin: false,
    login: '',
    password: '',
  };

  handleGetLogin = (event, value) => {
    this.setState({
      login: value,
      isErrorLogin: false,
    });
  };

  handleGetPass = (event, value) => {
    this.setState({
      password: value,
      isErrorPassword: false,
    });
  };

  checkLogin = () => {
    if (this.state.login === 'admin') {
      return true;
    }

    this.setState({
      isErrorLogin: true,
    });

    return false;
  };

  checkPassword = () => {
    if (this.state.password === 'admin') {
      return true;
    }

    this.setState({
      isErrorPassword: true,
    });

    return false;
  };

  handleCheckUser = () => {
    if (this.checkLogin() && this.checkPassword()) {
      this.props.logIn();
    }
  };

  render() {
    const colorBorderLogin = this.state.isErrorLogin ? 'red' : '#dedede';
    const colorBorderPassword = this.state.isErrorPassword ? 'red' : '#dedede';

    return (
      <div className={'login'}>
        <div className={'login-block'}>
          <div className={'login-head'}>
            <img src={logo}/>
          </div>
          <div className={'login-form'}>
            <div className={'input-block'}>
              <div className={'title'}>
                <h3>{'Login'}</h3>
              </div>
              <TextField
                name="user-registration"
                placeholder=""
                type="text"
                underlineFocusStyle={{ borderColor: '#faae42' }}
                underlineStyle={{ borderColor: colorBorderLogin }}
                onChange={this.handleGetLogin}
              />
              {
                this.state.isErrorLogin &&
                  <div className={'error-message'}>
                    {'Please check your login!'}
                  </div>
              }
            </div>
            <div className={'input-block'}>
              <div className={'title'}>
                <h3>{'Password'}</h3>
              </div>
              <TextField
                name="pass-registration"
                placeholder=""
                type="password"
                underlineFocusStyle={{ borderColor: '#faae42' }}
                underlineStyle={{ borderColor: colorBorderPassword }}
                onChange={this.handleGetPass}
              />
              {
                this.state.isErrorPassword &&
                <div className={'error-message'}>
                  {'Please check your password!'}
                </div>
              }
            </div>
            <div className={'buttons-block'}>
              <RaisedButton
                backgroundColor="#f5a623"
                className={'button'}
                label={button.logIn}
                labelStyle={{ color: '#ffffff' }}
                style={{ minWidth: 207 }}
                onClick={this.handleCheckUser}
              />
            </div>
          </div>
        </div>
        {this.props.authorized && <Redirect to={'/'}/> }
      </div>
    );
  }
}

Login.propTypes = propTypes;

const mapStateToProps = (state) => ({
  authorized: state.authorization.authorized,
});

const mapDispatchToProps = (dispatch) => ({
  logIn: () => dispatch(logIn()),
});

export default connect(mapStateToProps, mapDispatchToProps)(Login);
