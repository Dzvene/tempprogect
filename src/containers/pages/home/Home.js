/* eslint-disable complexity */
import { PureComponent } from 'react';
import { Table, TableBody, TableHeader, TableHeaderColumn, TableRow, TableRowColumn } from 'material-ui/Table';
import axios from 'axios';
import { baseURL } from '../../../constants/API';

const spinner = require('./../../../assets/images/spinner.svg'); // eslint-disable-line

import './styles.less';

const propTypes = {};

class Home extends PureComponent {
  state = {
    data: [],
    showSpinner: true,
  };

  componentDidMount() {

  }


  fetchData = () => {
    const url = `${baseURL}/data`;

    axios.get(url).
      then(this.handleResponseReceive).
      catch(this.handleErrorReceive);
  };

  handleResponseReceive = ({ data }) => {
    this.setState({
      data,
      showSpinner: false,
    });
  };

  handleErrorReceive = (data) => {
    this.setState({
      showSpinner: false,
    });

    console.error(`Error response! ${data}`);
  };

  render() {
    return (
      <div
        className={'home'}
        style={{ height: 'calc(100% - 65px)' }}
      >
        <div
          className={'content home-table'}
          style={{ height: 'calc(100% - 110px)' }}
        >
          <Table
            bodyStyle={{ height: 'calc(100% - 115px)', maxHeight: 148, overflowY: 'overlay', padding: '0 2px' }}
            className={'table in-popup table-main-header'}
            fixedHeader
            headerStyle={{ fontSize: '16px', padding: '0 2px' }}
            selectable={false}
            wrapperStyle={{
              marginBottom: -2,
            }}
          >
            <TableHeader
              adjustForCheckbox={false}
              className={'header'}
              displaySelectAll={false}
            >
              <TableRow>
                <TableHeaderColumn
                  className={'row'}
                  colSpan="4"
                  style={{
                    textAlign: 'left',
                    fontSize: '20px',
                  }}
                >
                  {'Some data'}
                </TableHeaderColumn>
              </TableRow>
            </TableHeader>
          </Table>
          <Table
            bodyStyle={{ height: 'calc(100% - 115px)', overflowY: 'overlay', padding: '0 2px' }}
            className={'table in-popup'}
            fixedHeader
            headerStyle={{ fontSize: '16px', padding: '0 2px', marginBottom: '-1px' }}
            selectable={false}
            wrapperStyle={{
              height: '100%',
            }}
          >
            <TableHeader
              adjustForCheckbox={false}
              className={'header'}
              displaySelectAll={false}
            >
              <TableRow>
                <TableHeaderColumn className={'column'}>{'Name'}</TableHeaderColumn>
                <TableHeaderColumn className={'column'}>{'Type'}</TableHeaderColumn>
                <TableHeaderColumn className={'column'}>{'Version'}</TableHeaderColumn>
                <TableHeaderColumn className={'column'}>{'Status'}</TableHeaderColumn>
              </TableRow>
            </TableHeader>
            <TableBody
              className={'body'}
              displayRowCheckbox={false}
            >
              {this.state.data.map((row, index) => (
                <TableRow key={index}>
                  <TableRowColumn className={'column'}>{row.name}</TableRowColumn>
                  <TableRowColumn className={'column'}>{row.cluster.nodes_size}</TableRowColumn>
                  <TableRowColumn className={'column'}>{'Bluvalt'}</TableRowColumn>
                  {this.renderColumnStatus(row.cluster.status, row.id)}
                  {this.renderRowColumnsButtons(row)}
                </TableRow>
              ))}
              {
                (this.state.data.length <= 0) &&
                <TableRow
                  colSpan="5"
                >
                  <TableHeaderColumn
                    className={'column'}
                    style={{
                      textAlign: 'center',
                    }}
                  >
                    {this.state.showSpinner ? <img src={spinner} style={{ padding: 40 }}/> : 'No data!'}
                  </TableHeaderColumn>
                </TableRow>
              }
            </TableBody>
          </Table>
        </div>
      </div>
    );
  }
}

Home.propTypes = propTypes;

export default Home;

