/* eslint-disable no-case-declarations */

import { LOG_IN, LOG_OUT } from '../actions/autorization';

const initialState = {
  authorized: false,
};

const loginReducer = (state = initialState, action) => {
  switch (action.type) {
    case LOG_IN:
      const isLogin = { authorized: true };

      return { ...state, ...isLogin };
    case LOG_OUT:
      const isLogOut = { authorized: false };

      return { ...state, ...isLogOut };
    default:
      return state;
  }
};

export default loginReducer;
