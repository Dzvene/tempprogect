import { combineReducers } from 'redux';
import { routerReducer } from 'react-router-redux';

import loginReducer from './reducers/authorization';

export default combineReducers({
  routing: routerReducer,
  authorization: loginReducer,
});
