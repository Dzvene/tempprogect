export const LOG_IN = 'LOG_IN';
export const LOG_OUT = 'LOG_OUT';

export const logIn = (authorized) => ({
  type: LOG_IN,
  authorized,
});

export const logOut = (authorized) => ({
  type: LOG_OUT,
  authorized,
});
