export const button = {
  logIn: 'Log In',
};

export const pagesUrl = {
  index: '/',
  home: '/',
  login: '/login',
};
